ansible-role-hashicorp-vault
=========

Install and configure a HashiCorp Vault Cluster.

During the initial Ansible execution, the cluster is initialized, and the `Vault Unseal Keys` and `Vault Root Token` are saved in the `/root` directory on the first Vault node.

To update a specific Vault node to a newer version, change the vault_version variable and use the ``--limit` option during execution. Make sure to unseal the Vault after the update:

```bash
ansible-playbook -i <inventory_path> <playbook_path> --limit <vault_fqdn>
```
Requirements
------------
**DNS Records:**
  - DNS records for all Vault nodes and a common name if you plan to use a load balancer.
  - You may skip the DNS records and use IP addresses in the `vault_cluster_nodes` variable instead. Ensure the IP addresses are included in the certificates.

**Certificates:**

Specify whether an SSL certificate should be copied from the Ansible control node to the remote hosts, or if an existing certificate is already present on the remote host. Choose one of these methods by specifying the following inventory variables, or skip them to use HTTP only:

```yml
# Choose one of theese two methods

# Copy a tls certificate from the Ansible control node to the remote server
copy_vault_tls_certificate: "<inventory_path>/group_vars/secrets/vault.crt"
copy_vault_tls_certificate_key: "<inventory_path>/group_vars/secrets/vault.key"

# Use an existing cert that is already present on the remote server
existing_vault_tls_certificate: "/etc/ssl/private/vault.crt"
existing_vault_tls_certificate_key: "/etc/ssl/private/vault.key
```

Example OpenSSL SAN Configuration:
```md
subjectAltName = @alt_names
[alt_names]
DNS.1 = vault.example.com
DNS.2 = app-vault-01.example.com
DNS.3 = app-vault-02.example.com
DNS.4 = app-vault-03.example.com
```

**Required collections:**
  - community.general

Role Variables
--------------
The default values for the variables are set in `defaults/main.yml`:
```yml
# Defaults file for hashicorp-vault
vault_version: "latest"  # Version to be installed. Specify a specific version (e.g., 1.12.3-1)
vault_key_shares: 3  # Maximum number of Vault unseal keys
vault_key_threshold: 2  # Minimum number of keys required to unseal Vault
bind_api_address: 0.0.0.0  # Bind IP for Vault API (0.0.0.0 = any)
vault_api_port: 8200  # Vault API port
bind_cluster_address: 0.0.0.0  # Bind IP for Vault cluster operations
vault_cluster_port: 8201  # Vault cluster port
vault_backend_path: /opt/vault  # Vault configuration and data directory
tls_min_version: tls12  # Minimum TLS version
tls_disable: false  # Use HTTP only when tls_disable is true
vault_log_level: "Info"  # Audit log level
vault_ui: "true"  # Enable Web UI

# Vault unseal keys and root token will be saved to the following files on the first cluster node. This applies only once during cluster initialization
vault_unseal_keys_file: /root/.vault-unseal-keys
vault_root_token_file: /root/.vault-token

# All cluster nodes (Provide a list with FQDNs or IP addresses of all cluster nodes. Ensure they are included in the certificates if TLS is enabled)
vault_cluster_nodes: []
#  - vault-01.example.com
#  - vault-02.example.com
#  - vault-03.example.com
```

Example Inventory
-----------------
```yml
all:
  hosts:
  children:
    vault:
      hosts:
        app-vault-01.example.com:
          ansible_host: 10.199.34.114
        app-vault-02.example.com:
          ansible_host: 10.199.34.115
        app-vault-03.example.com:
          ansible_host: 10.199.34.116
```

Example Playbook
----------------

```yml
---
# Install and configure a Vault Cluster with existing certificates on the remote servers
- hosts: vault
  become: yes
  gather_facts: yes
  roles:
    - role: hashicorp-vault
      vars:
        existing_vault_tls_certificate: '/etc/ssl/private/default.crt'
        existing_vault_tls_certificate_key: '/etc/ssl/private/default.key'
        vault_version: "1.16.0-1"
        vault_cluster_nodes:
          - vault-01.example.com
          - vault-02.example.com
          - vault-03.example.com
```

License
-------

GPLv3

Author Information
------------------

Oleg Franko
