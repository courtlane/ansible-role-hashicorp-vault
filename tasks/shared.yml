- name: Import Hashicorp Repository and GPG Key
  block:
    - name: Import Hashicorp GPG Key
      ansible.builtin.shell:
        cmd: wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg 

    - name: Import Hashicorp Repository
      ansible.builtin.apt_repository:
        repo: "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com {{ansible_distribution_release}} main"
        state: present
        filename: hashicorp

- name: Install Vault latest Package
  apt:
    name: vault
    state: present
    update_cache: yes
  notify: restart vault
  when: vault_version == 'latest'

- name: Install Vault Package
  apt:
    name: "vault={{ vault_version }}"
    state: present
    allow_downgrade: yes
    update_cache: yes
  notify: restart vault
  when: vault_version != 'latest'

- name: Copy TLS Certificates to Vault Server
  copy:
    src: "{{item}}" 
    dest: "{{ vault_backend_path }}/tls/{{ item | basename }}"
    owner: vault
    group: vault
    mode: '0600'
  with_items: 
    - "{{ copy_vault_tls_certificate }}"
    - "{{ copy_vault_tls_certificate_key }}"
  when:
    - tls_disable == false
    - copy_vault_tls_certificate is defined
    - copy_vault_tls_certificate_key is defined
  notify: restart vault

- name: Add vault user to ssl-cert group
  user:
    name: vault
    groups: "ssl-cert"
    append: yes
  when:
    - tls_disable == false
    - existing_vault_tls_certificate is defined
    - existing_vault_tls_certificate_key is defined

- name: Create a symbolic link to existing certificate
  file:
    src: "{{ item }}"
    dest: "{{ vault_backend_path }}/tls/{{ item | basename }}"
    state: link
  with_items:
    - "{{ existing_vault_tls_certificate }}"
    - "{{ existing_vault_tls_certificate_key }}"
  when:
    - tls_disable == false
    - existing_vault_tls_certificate is defined
    - existing_vault_tls_certificate_key is defined
  notify: restart vault

- name: Set fact for copied certificates
  set_fact:
    vault_tls_certificate: "{{ copy_vault_tls_certificate }}"
    vault_tls_certificate_key: "{{ copy_vault_tls_certificate_key }}"
  when:
    - copy_vault_tls_certificate is defined
    - copy_vault_tls_certificate_key is defined

- name: Set fact for existing certificates
  set_fact:
    vault_tls_certificate: "{{ existing_vault_tls_certificate }}"
    vault_tls_certificate_key: "{{ existing_vault_tls_certificate_key }}"
  when:
    - existing_vault_tls_certificate is defined
    - existing_vault_tls_certificate_key is defined

- name: Create Vault Configuration from Template
  ansible.builtin.template:
    src: vault.hcl.j2
    dest: /etc/vault.d/vault.hcl
    mode: '0644'
    remote_src: no
  notify: restart vault

- name: Start Vault Service
  ansible.builtin.systemd:
    name: vault
    state: started
    enabled: yes

- name: Flush handlers
  meta: flush_handlers

- name: save VAULT_ADDR to /etc/profile.d
  ansible.builtin.lineinfile:
    regexp: "^export VAULT_ADDR="
    path: /etc/profile.d/vault.sh
    create: yes
    line: "export VAULT_ADDR={{ vault_api_addr }}"
    mode: "0644"
    owner: root
    group: root

- name: Check Vault Status
  ansible.builtin.command:
    cmd: vault status -format=yaml
  environment:
    VAULT_ADDR: "{{vault_api_addr}}"
  register: vault_status_raw
  changed_when: no
  check_mode: no
  failed_when:
    - vault_status_raw.rc == 1

- name: Save Vault Status
  ansible.builtin.set_fact:
    vault_status: "{{ vault_status_raw.stdout | from_yaml }}"

- name: Get the index of inventory_host from ansible_play_hosts
  ansible.builtin.set_fact:
    host_index: "{{ lookup('ansible.utils.index_of', ansible_play_hosts, 'eq', \"{{ inventory_hostname }}\") }}"